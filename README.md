The Original LambTracker manual was being written in Scrivener. The entire manual was a wip that has been overcome by events. This repo will be going away once I've extracted out the useful bits to be included in the newer Lambtracker documentation that will be available on-line. LambTracker as a sysmte is being slowly replaced by AnimalTrakker. Eventually this will be archived or delet4ed whichever seems appropriate when it's time. 

### Scrivener Starter Repository

### License

    Copyright 2012-2014 Roy Liu

    Licensed under the Apache License, Version 2.0 (the "License"); you may not
    use this file except in compliance with the License. You may obtain a copy
    of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
    License for the specific language governing permissions and limitations
    under the License.
